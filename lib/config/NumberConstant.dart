class NumberConstant {
  // TextField
  static double get defaultTextFieldHeight => 50;

  // Button
  static double get defaultNormalButtonHeight => 50;

  // EdgeInsect
  static double get smallEdgeInsect => 10;
  static double get mediumEdgeInsect => 20;
  static double get largeEdgeInsect => 30;

  // PreferredSize
  static double get accountPreferredSizeHeight => 55;
  static double get notificationPreferredSizeHeight => 160;
  static double get transferAppBarSizeHeight => 150;
  static double get homeAppBarSizeHeight => 450;
  static double get accountAppBarSizeHeight => 370;
  static double get directoryHeight => 380;
  static double get directoryListHeight => 210;
  static double get directoryDetailHeight => 350;

  // Widget
  static double get listLatestTransactionItemHeight => 117;
  static double get listItemSettingHeight => 60;
}
