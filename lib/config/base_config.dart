import 'dart:io';

class BaseConfig {
  static const URL_SERVER = "https://crisadevel.daksa.co.id/switching";
  static const SECRET_KEY = "ccf2197b6d9445a3a6f1e1937a4da4b2";
  static const API_KEY = "59bc2f367a7b4224b76c441633a61690";

  static final String appVersion = Platform.isIOS ? '1.0.1' : '1.0.1';
  static const String company_name = 'Daksa Mobile Banking © 2021';

  static const String developerName = '© 2021 Daksa ';
  static const contentType = 'application/json';
}