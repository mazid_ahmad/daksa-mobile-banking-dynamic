class CanonicalPath {
  static const URL_PREFIX = "/daksa/mobile";
  static const LOGIN_BY_OTP = "$URL_PREFIX/login/otp";
  static const LOGIN_BY_USERNAME = "$URL_PREFIX/login/otp";
  static const VERIFY_OTP = "$LOGIN_BY_OTP/verify";
  static const HOME_WIDGET = "$URL_PREFIX/ui/home";
}
