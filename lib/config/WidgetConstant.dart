class WidgetConstant {

  // Main Page
  static const int MAIN_HOME_WIDGET = 0;
  static const int MAIN_ACCOUNT_WIDGET = 1;
  static const int MAIN_NOTIFICATION_WIDGET = 2;
  static const int MAIN_SETTING_WIDGET = 3;

  // Register Page
  static const int REGISTER_AGREEMENT_WIDGET = 0;
  static const int REGISTER_IDENTITY_WIDGET = 1;
  static const int REGISTER_ACCOUNT_WIDGET = 2;
}