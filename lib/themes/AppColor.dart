import 'package:flutter/material.dart';

class AppColor {
  static final AppColor _instance = AppColor._internal();

  AppColor._internal();

  Color primary = Color(0xFF008AE0);
  Color primaryVariant = Color(0xFF01A6FF);
  Color onPrimary = Colors.white;
  Color secondary = Color(0xFF00AFB1);
  Color secondaryVariant = Color(0xFF008B8C);
  Color onSecondary = Colors.white;
  Color onError = Color(0xFFB00020);
  Color onSurface = Color(0xFF333333);
  Color onBackground = Color(0xFF333333);
  Color lightGrey = Color(0xFFF0F1F4);
  Color grey = Color(0xFF8A98AC);

  factory AppColor() {
    return _instance;
  }
}
