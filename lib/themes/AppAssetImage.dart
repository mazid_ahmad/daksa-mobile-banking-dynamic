import 'package:flutter/material.dart';

class AppAssetImage {
  // Illustration
  static AssetImage get wallet => AssetImage("assets/images/illustrations/wallet.png");
  static AssetImage get loginEmail => AssetImage("assets/images/illustrations/login_email.png");
  static AssetImage get loginPhone => AssetImage("assets/images/illustrations/login_phone.png");
  static AssetImage get confirmOTP => AssetImage("assets/images/illustrations/confirm_otp.png");
  static AssetImage get directory => AssetImage("assets/images/illustrations/directory.png");

  //Logo
  static AssetImage get dmbLogoWhite  => AssetImage("assets/images/logos/dmb_logo_white.png");
  static AssetImage get dmbLogo  => AssetImage("assets/images/logos/dmb_logo.png");
  static AssetImage get daksaLogo  => AssetImage("assets/images/logos/daksa_logo.png");

  // Picture
  static AssetImage get banner => AssetImage("assets/images/pictures/banner.jpeg");

  // Etc
  static AssetImage get bottomImage => AssetImage("assets/images/etc/bottom_image.png");
}