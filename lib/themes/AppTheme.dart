import 'package:dmb_dynamic_home_example/themes/AppColor.dart';
import 'package:flutter/material.dart';

class AppTheme {
  static ThemeData get light => ThemeData(
        colorScheme: ColorScheme(
            primary: AppColor().primary,
            primaryVariant: AppColor().primaryVariant,
            secondary: AppColor().secondary,
            secondaryVariant: AppColor().primaryVariant,
            surface: Colors.white,
            background: Colors.white,
            error: AppColor().onError,
            onPrimary: AppColor().onPrimary,
            onSecondary: AppColor().onSecondary,
            onSurface: AppColor().onSurface,
            onBackground: AppColor().onBackground,
            onError: AppColor().onError,
            brightness: Brightness.light),
        visualDensity: VisualDensity.adaptivePlatformDensity,
        fontFamily: 'Poppins',
        textTheme: TextTheme(
          headline1: TextStyle(
            fontSize: 96.0,
            fontWeight: FontWeight.w300,
            letterSpacing: -1.5,
          ),
          headline2: TextStyle(
            fontSize: 60.0,
            fontWeight: FontWeight.w300,
            letterSpacing: -0.5,
          ),
          // H3: modified style
          headline3: TextStyle(
            fontSize: 42.0,
            fontWeight: FontWeight.bold,
            letterSpacing: -0.24,
            height: 1.0,
          ),
          // H4: modified style
          headline4: TextStyle(
            fontSize: 24.0,
            fontWeight: FontWeight.w600,
            letterSpacing: -0.24,
            height: 1.2,
          ),
          // H5: modified style
          headline5: TextStyle(
            fontSize: 20.0,
            fontWeight: FontWeight.w600,
            letterSpacing: -0.24,
            height: 1.0,
          ),
          // H6: modified style
          headline6: TextStyle(
            fontSize: 18.0,
            fontWeight: FontWeight.w600,
            letterSpacing: -0.24,
            height: 1.2,
          ),
          subtitle1: TextStyle(
            fontSize: 16.0,
            fontWeight: FontWeight.w500,
            letterSpacing: 0.15,
          ),
          // sub2: modified style
          subtitle2: TextStyle(
            fontSize: 14.0,
            fontWeight: FontWeight.w500,
            letterSpacing: -0.24,
          ),
          bodyText1: TextStyle(
            fontSize: 16.0,
            fontWeight: FontWeight.normal,
            letterSpacing: 0.5,
          ),
          // body2: modified style
          bodyText2: TextStyle(
            fontSize: 15.0,
            fontWeight: FontWeight.normal,
            letterSpacing: 0.0,
            height: 1.2,
          ),
          // button: modified style
          button: TextStyle(
            fontSize: 16.0,
            fontWeight: FontWeight.w600,
            letterSpacing: 1.25,
          ),
          caption: TextStyle(
            fontSize: 12.0,
            fontWeight: FontWeight.normal,
            letterSpacing: 0.4,
          ),
          overline: TextStyle(
            fontSize: 10.0,
            fontWeight: FontWeight.normal,
            letterSpacing: 1.5,
          ),
        ),
      );
}
