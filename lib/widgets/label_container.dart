import 'package:dmb_dynamic_home_example/themes/AppColor.dart';
import 'package:flutter/material.dart';

class LabelContainer extends StatelessWidget {
  final IconData? prefixIcon;
  final IconData? suffixIcon;
  final String? label;

  const LabelContainer(
      {Key? key, this.prefixIcon, this.suffixIcon, @required this.label})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
        decoration: BoxDecoration(
            color: AppColor().onPrimary.withOpacity(0.15),
            borderRadius: BorderRadius.circular(20)),
        child: Row(
          children: [
            prefixIcon != null
                ? Icon(
                    prefixIcon,
                    color: AppColor().onPrimary,
                    size: 15,
                  )
                : Container(),
            Text(label!,
                style: TextStyle(fontSize: 12, color: AppColor().onPrimary)),
            suffixIcon != null
                ? Icon(
                    suffixIcon,
                    color: AppColor().onPrimary,
                    size: 15,
                  )
                : Container()
          ],
        ));
  }
}
