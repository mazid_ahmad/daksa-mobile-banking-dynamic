import 'package:dmb_dynamic_home_example/themes/AppColor.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class ListItemHistoryTransaction extends StatelessWidget {
  final String? type;
  final String? amount;
  final DateTime? date;
  final Function()? onPressed;

  const ListItemHistoryTransaction(
      {Key? key,
      @required this.type,
      @required this.amount,
      @required this.date,
      @required this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.symmetric(vertical: 2),
      width: screenSize.width,
      height: 80,
      child: Material(
        color: AppColor().onPrimary,
        child: InkWell(
          onTap: onPressed,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 25),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      type!,
                      style: TextStyle(
                          color: AppColor().primary,
                          fontWeight: FontWeight.bold,
                          fontSize: 16),
                    ),
                    Text(
                      date != null
                          ? DateFormat("d MMM yyyy HH:mm").format(date!)
                          : "-",
                      style: TextStyle(
                          color: AppColor().secondary,
                          fontWeight: FontWeight.w600,
                          fontSize: 16),
                    ),
                  ],
                ),
                Text(
                  "Rp ${amount!}",
                  style: TextStyle(
                      color: AppColor().secondary,
                      fontWeight: FontWeight.w600,
                      fontSize: 16),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
