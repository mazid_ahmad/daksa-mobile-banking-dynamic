import 'package:dmb_dynamic_home_example/themes/AppColor.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class ListItemNotification extends StatelessWidget {
  final String? title;
  final DateTime? date;
  final void Function()? onPressed;

  const ListItemNotification({Key? key, this.title, this.date, this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 2),
      child: Material(
        color: AppColor().onPrimary,
        child: InkWell(
          onTap: onPressed,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 30, vertical: 15),
            width: screenSize.width,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  title ?? "",
                  style: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: 15,
                      color: AppColor().onBackground),
                ),
                SizedBox(height: 5),
                if (date != null) Text(DateFormat("dd MMM yyyy").format(date!))
              ],
            ),
          ),
        ),
      ),
    );
  }
}
