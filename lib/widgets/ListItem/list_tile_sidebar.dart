import 'package:dmb_dynamic_home_example/themes/AppColor.dart';
import 'package:flutter/material.dart';

class ListTileSidebar extends StatelessWidget {
  final String label;
  final void Function()? onTap;
  const ListTileSidebar({Key? key, required this.label, this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Padding(
        padding: const EdgeInsets.only(left: 15),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(label,
                style: TextStyle(
                    color: AppColor().primary,
                    fontWeight: FontWeight.w600,
                    fontSize: 18)),
            Icon(Icons.chevron_right, color: AppColor().primary)
          ],
        ),
      ),
      onTap: onTap,
    );
  }
}
