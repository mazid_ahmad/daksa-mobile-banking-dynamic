import 'package:dmb_dynamic_home_example/themes/AppColor.dart';
import 'package:flutter/material.dart';

class ListContactItem extends StatelessWidget {
  final String? name;
  final String? number;

  const ListContactItem({Key? key, this.name, this.number}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColor().onPrimary,
      margin: EdgeInsets.symmetric(vertical: 1),
      padding: EdgeInsets.symmetric(horizontal: 30, vertical: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                name ?? "",
                style: TextStyle(
                    fontSize: 18,
                    color: AppColor().primary,
                    fontWeight: FontWeight.w600),
              ),
              SizedBox(height: 3),
              Text(number ?? "", style: TextStyle(fontSize: 15))
            ],
          ),
          ElevatedButton(onPressed: () {}, child: Text("Invite"))
        ],
      ),
    );
  }
}
