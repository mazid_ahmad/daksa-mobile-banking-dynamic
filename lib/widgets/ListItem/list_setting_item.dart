import 'package:dmb_dynamic_home_example/config/NumberConstant.dart';
import 'package:dmb_dynamic_home_example/themes/AppColor.dart';
import 'package:flutter/material.dart';

class ListItemSetting extends StatelessWidget {
  final String? label;
  final String? description;
  final String? value;
  final bool? switchValue;
  final Function()? onPressed;
  final Function(bool? value)? onChange;

  const ListItemSetting(
      {Key? key,
      this.label,
      this.description,
      this.value,
      this.switchValue,
      this.onPressed,
      this.onChange})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.symmetric(vertical: 1),
      width: screenSize.width,
      height: NumberConstant.listItemSettingHeight,
      child: Material(
        color: AppColor().onPrimary,
        child: InkWell(
          onTap: onPressed,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 30),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      label!,
                      style: TextStyle(
                          color: AppColor().primary,
                          fontWeight: FontWeight.w500,
                          fontSize: 16),
                    ),
                    description != null ? SizedBox(height: 5) : SizedBox(),
                    description != null
                        ? Text(
                            description!,
                            style: TextStyle(
                                color: AppColor().grey.withOpacity(0.8),
                                fontWeight: FontWeight.w500,
                                fontSize: 12),
                          )
                        : SizedBox(),
                  ],
                ),
                Container(
                  child: switchValue == null
                      ? Row(
                          children: [
                            value != null
                                ? Text(
                                    value!,
                                    style: TextStyle(
                                        color: AppColor().primary,
                                        fontWeight: FontWeight.w500,
                                        fontSize: 16),
                                  )
                                : SizedBox(),
                            Icon(Icons.keyboard_arrow_right_rounded,
                                color: AppColor().primary)
                          ],
                        )
                      : Switch(
                          value: switchValue ?? false, onChanged: onChange),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
