import 'package:dmb_dynamic_home_example/config/NumberConstant.dart';
import 'package:dmb_dynamic_home_example/themes/AppColor.dart';
import 'package:flutter/material.dart';

class ListItemLatestTransaction extends StatelessWidget {
  final String? type;
  final String? amount;
  final String? ref;
  final String? destination;
  final String? date;
  final Function()? onPressed;

  const ListItemLatestTransaction(
      {Key? key,
      @required this.type,
      @required this.amount,
      @required this.ref,
      @required this.destination,
      @required this.date,
      @required this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.symmetric(vertical: 2),
      width: screenSize.width,
      height: NumberConstant.listLatestTransactionItemHeight,
      child: Material(
        color: AppColor().onPrimary,
        child: InkWell(
          onTap: () {},
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 25),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      type!,
                      style: TextStyle(
                          color: AppColor().primary,
                          fontWeight: FontWeight.bold,
                          fontSize: 16),
                    ),
                    Text(
                      date!,
                      style: TextStyle(
                          color: AppColor().secondary,
                          fontWeight: FontWeight.w600,
                          fontSize: 16),
                    ),
                  ],
                ),
                SizedBox(height: 10),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Amount : $amount"),
                        Text("Ref : $ref"),
                        Text("Destination : $destination"),
                      ],
                    ),
                    Row(
                      children: [
                        Icon(Icons.check_circle_outline_rounded,
                            color: AppColor().primary),
                        SizedBox(width: 5),
                        Text(
                          "Success",
                          style: TextStyle(
                              color: AppColor().primary,
                              fontWeight: FontWeight.bold,
                              fontSize: 16),
                        ),
                      ],
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
