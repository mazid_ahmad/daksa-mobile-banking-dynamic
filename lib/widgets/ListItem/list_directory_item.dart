import 'package:dmb_dynamic_home_example/config/NumberConstant.dart';
import 'package:dmb_dynamic_home_example/themes/AppColor.dart';
import 'package:flutter/material.dart';

class ListDirectoryItem extends StatelessWidget {
  final String? name;
  final ImageProvider? image;
  final String? address;
  final String? phone;
  final Function()? onPressed;

  const ListDirectoryItem(
      {Key? key,
      @required this.name,
      this.image,
      this.address,
      this.phone,
      this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.symmetric(vertical: 2),
      width: screenSize.width,
      height: NumberConstant.listLatestTransactionItemHeight,
      child: Material(
        color: AppColor().onPrimary,
        child: InkWell(
          onTap: onPressed,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 25, vertical: 20),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                image != null
                    ? Image(image: image!, width: 50)
                    : Container(width: 50),
                SizedBox(width: 20),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(name!,
                            overflow: TextOverflow.clip,
                            style: TextStyle(
                                fontWeight: FontWeight.w600, fontSize: 16)),
                        SizedBox(height: 5),
                        Text(address!,
                            overflow: TextOverflow.clip,
                            style: TextStyle(fontWeight: FontWeight.w500)),
                        SizedBox(height: 5),
                        Text(phone!),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
