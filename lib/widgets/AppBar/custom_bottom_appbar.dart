import 'package:dmb_dynamic_home_example/config/WidgetConstant.dart';
import 'package:dmb_dynamic_home_example/themes/AppColor.dart';
import 'package:dmb_dynamic_home_example/widgets/Button/bottom_appbar_button.dart';
import 'package:flutter/material.dart';

class CustomBottomAppBar extends StatefulWidget {
  final int? currentIndex;
  final Function(int? value)? onHomePressed;
  final Function(int? value)? onAccountPressed;
  final Function(int? value)? onNotificationPressed;
  final Function(int? value)? onSettingPressed;

  const CustomBottomAppBar(
      {Key? key,
      this.currentIndex,
      this.onHomePressed,
      this.onAccountPressed,
      this.onNotificationPressed,
      this.onSettingPressed})
      : super(key: key);

  @override
  _CustomBottomAppBarState createState() => _CustomBottomAppBarState();
}

final iconListActive = <IconData>[
  Icons.home,
  Icons.account_circle,
  Icons.notifications,
  Icons.settings,
];

final iconListInactive = <IconData>[
  Icons.home_outlined,
  Icons.account_circle_outlined,
  Icons.notifications_none_rounded,
  Icons.settings,
];

class _CustomBottomAppBarState extends State<CustomBottomAppBar> {
  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    return BottomAppBar(
      color: AppColor().primary,
      clipBehavior: Clip.antiAlias,
      shape: CircularNotchedRectangle(),
      notchMargin: 10,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Container(
            padding: EdgeInsets.symmetric(horizontal: 10),
            width: screenSize.width / 2.5,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                BottomAppbarButton(
                    label: "Home",
                    onPressed: () {
                      widget.onHomePressed!(WidgetConstant.MAIN_HOME_WIDGET);
                    },
                    iconData: widget.currentIndex ==
                            WidgetConstant.MAIN_HOME_WIDGET
                        ? iconListActive[WidgetConstant.MAIN_HOME_WIDGET]
                        : iconListInactive[WidgetConstant.MAIN_HOME_WIDGET]),
                BottomAppbarButton(
                    label: "Account",
                    onPressed: () {
                      widget.onHomePressed!(WidgetConstant.MAIN_ACCOUNT_WIDGET);
                    },
                    iconData: widget.currentIndex ==
                            WidgetConstant.MAIN_ACCOUNT_WIDGET
                        ? iconListActive[WidgetConstant.MAIN_ACCOUNT_WIDGET]
                        : iconListInactive[WidgetConstant.MAIN_ACCOUNT_WIDGET]),
              ],
            ),
          ),
          SizedBox(width: 50),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 10),
            width: screenSize.width / 2.5,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                BottomAppbarButton(
                    label: "Notification",
                    onPressed: () {
                      widget.onHomePressed!(
                          WidgetConstant.MAIN_NOTIFICATION_WIDGET);
                    },
                    iconData: widget.currentIndex ==
                            WidgetConstant.MAIN_NOTIFICATION_WIDGET
                        ? iconListActive[
                            WidgetConstant.MAIN_NOTIFICATION_WIDGET]
                        : iconListInactive[
                            WidgetConstant.MAIN_NOTIFICATION_WIDGET]),
                BottomAppbarButton(
                    label: "Setting",
                    onPressed: () {
                      widget.onHomePressed!(WidgetConstant.MAIN_SETTING_WIDGET);
                    },
                    iconData: widget.currentIndex ==
                            WidgetConstant.MAIN_SETTING_WIDGET
                        ? iconListActive[WidgetConstant.MAIN_SETTING_WIDGET]
                        : iconListInactive[WidgetConstant.MAIN_SETTING_WIDGET]),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
