import 'dart:convert';

import 'package:dmb_dynamic_home_example/themes/AppColor.dart';
import 'package:dynamic_widget/dynamic_widget.dart';
import 'package:dynamic_widget/dynamic_widget/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';

class FeatureCardHomeParser extends WidgetParser {
  @override
  Map<String, dynamic>? export(Widget? widget, BuildContext? buildContext) {
    throw UnimplementedError();
  }

  @override
  Widget parse(Map<String, dynamic> map, BuildContext buildContext,
      ClickListener? listener) {
    var screenSize = MediaQuery.of(buildContext).size;
    var widthMargin = map["width_margin"];
    var padding = map["padding"];
    var data = map["child"];
    var color = map["color"];

    var widget = Container(
      width: widthMargin != null ? screenSize.width - widthMargin : screenSize.width,
      padding: padding != null ? parseEdgeInsetsGeometry(padding) : null,
      decoration: BoxDecoration(
          color: color != null ? parseHexColor(color) : AppColor().onPrimary,
          borderRadius: BorderRadius.circular(20),
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.05),
              spreadRadius: 5,
              blurRadius: 15,
              offset: Offset(
                  0, 8), // changes position of shadow
            ),
          ]),
      child: DynamicWidgetBuilder.build(
          json.encode(data), buildContext, NonResponseWidgetClickListener()),
    );
    return widget;
  }

  @override
  String get widgetName => "FeatureCardHome";

  @override
  Type get widgetType => throw UnimplementedError();
}
