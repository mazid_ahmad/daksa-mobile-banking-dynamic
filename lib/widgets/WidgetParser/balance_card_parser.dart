import 'package:dmb_dynamic_home_example/pages/cubit/home_cubit.dart';
import 'package:dynamic_widget/dynamic_widget.dart';
import 'package:dynamic_widget/dynamic_widget/utils.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../balance_card.dart';

class BalanceCardParser extends WidgetParser {
  @override
  Map<String, dynamic>? export(Widget? widget, BuildContext? buildContext) {
    throw UnimplementedError();
  }

  @override
  Widget parse(Map<String, dynamic> map, BuildContext buildContext,
      ClickListener? listener) {
    HomeCubit _homeCubit = BlocProvider.of<HomeCubit>(buildContext);

    var state = _homeCubit.state;

    var card = BalanceCard(
      balance: state.balance,
      type: state.type,
      number: state.account,
      cardColor:
          map.containsKey('cardColor') ? parseHexColor(map['cardColor']) : null,
    );

    return card;
  }

  @override
  String get widgetName => "BalanceCard";

  @override
  Type get widgetType => throw UnimplementedError();
}
