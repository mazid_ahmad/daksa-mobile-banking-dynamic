import 'dart:convert';

import 'package:dmb_dynamic_home_example/widgets/Clipper/clip_path_class.dart';
import 'package:dynamic_widget/dynamic_widget.dart';
import 'package:flutter/cupertino.dart';

class CustomHomeClipPathParser extends WidgetParser {
  @override
  Map<String, dynamic>? export(Widget? widget, BuildContext? buildContext) {
    // TODO: implement export
    throw UnimplementedError();
  }

  @override
  Widget parse(Map<String, dynamic> map, BuildContext buildContext,
      ClickListener? listener) {
    var childWidget = map["child"];

    var widget = ClipPath(
      clipper: ClipPathClass(),
      child: childWidget != null
          ? DynamicWidgetBuilder.build(json.encode(childWidget), buildContext,
              NonResponseWidgetClickListener())
          : null,
    );

    return widget;
  }

  @override
  // TODO: implement widgetName
  String get widgetName => "CustomHomeClipPath";

  @override
  // TODO: implement widgetType
  Type get widgetType => throw UnimplementedError();
}
