import 'dart:convert';

import 'package:dmb_dynamic_home_example/pages/cubit/home_cubit.dart';
import 'package:dmb_dynamic_home_example/themes/AppColor.dart';
import 'package:dmb_dynamic_home_example/widgets/label_container.dart';
import 'package:dynamic_widget/dynamic_widget.dart';
import 'package:dynamic_widget/dynamic_widget/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomeHeaderParser extends WidgetParser {
  @override
  Map<String, dynamic>? export(Widget? widget, BuildContext? buildContext) {
    // TODO: implement export
    throw UnimplementedError();
  }

  @override
  Widget parse(Map<String, dynamic> map, BuildContext buildContext,
      ClickListener? listener) {
    HomeCubit _homeCubit = BlocProvider.of<HomeCubit>(buildContext);
    var state = _homeCubit.state;

    var backgroundColor = map["backgroundColor"];
    var padding = map["padding"];
    var balanceCard = map["balanceCard"];

    var widget = Container(
      color: backgroundColor != null
          ? parseHexColor(backgroundColor)
          : AppColor().primary,
      padding: padding != null ? parseEdgeInsetsGeometry(padding) : null,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(state.greeting ?? "greeting",
              style: TextStyle(color: AppColor().onPrimary)),
          Text(state.name ?? "name",
              style: TextStyle(
                  color: AppColor().onPrimary,
                  fontSize: 22,
                  fontWeight: FontWeight.w600)),
          SizedBox(height: 10),
          Row(
            children: [
              Text("Your Account",
                  style: TextStyle(color: AppColor().onPrimary)),
              SizedBox(width: 10),
              LabelContainer(
                  label: "Change", suffixIcon: Icons.keyboard_arrow_down)
            ],
          ),
          SizedBox(height: 10),
          if (balanceCard != null)
            DynamicWidgetBuilder.build(json.encode(balanceCard), buildContext,
                NonResponseWidgetClickListener()) ?? Container()
        ],
      ),
    );

    return widget;
  }

  @override
  // TODO: implement widgetName
  String get widgetName => "HomeHeader";

  @override
  // TODO: implement widgetType
  Type get widgetType => throw UnimplementedError();
}
