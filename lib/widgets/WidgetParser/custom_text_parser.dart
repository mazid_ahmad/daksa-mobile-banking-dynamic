import 'package:dynamic_widget/dynamic_widget.dart';
import 'package:dynamic_widget/dynamic_widget/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';

class CustomTextParser extends WidgetParser {
  final String? value;

  CustomTextParser({this.value});

  @override
  Map<String, dynamic>? export(Widget? widget, BuildContext? buildContext) {
    throw UnimplementedError();
  }

  @override
  Widget parse(Map<String, dynamic> map, BuildContext buildContext,
      ClickListener? listener) {
    var data = value ?? map["data"];
    var style = map["style"];

    var text =
        Text(data ?? "", style: style != null ? parseTextStyle(style) : null);
    return text;
  }

  @override
  String get widgetName => "CustomText";

  @override
  // TODO: implement widgetType
  Type get widgetType => throw UnimplementedError();
}
