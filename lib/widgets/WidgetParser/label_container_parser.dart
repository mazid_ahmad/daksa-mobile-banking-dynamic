import 'package:dynamic_widget/dynamic_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';

import '../label_container.dart';

class LabelContainerParser extends WidgetParser {
  final IconData? prefixIcon;
  final IconData? suffixIcon;
  final String? label;

  LabelContainerParser({this.prefixIcon, this.suffixIcon, this.label});

  @override
  Map<String, dynamic>? export(Widget? widget, BuildContext? buildContext) {
    // TODO: implement export
    throw UnimplementedError();
  }

  @override
  Widget parse(Map<String, dynamic> map, BuildContext buildContext,
      ClickListener? listener) {
    var widget = LabelContainer(
        label: label, suffixIcon: suffixIcon, prefixIcon: prefixIcon);
    return widget;
  }

  @override
  String get widgetName => "LabelContainer";

  @override
  // TODO: implement widgetType
  Type get widgetType => throw UnimplementedError();
}
