import 'dart:convert';

import 'package:dmb_dynamic_home_example/widgets/Button/feature_button.dart';
import 'package:dynamic_widget/dynamic_widget.dart';
import 'package:dynamic_widget/dynamic_widget/utils.dart';
import 'package:flutter/src/widgets/framework.dart';

class FeatureButtonParser extends WidgetParser {
  @override
  Map<String, dynamic>? export(Widget? widget, BuildContext? buildContext) {
    throw UnimplementedError();
  }

  @override
  Widget parse(Map<String, dynamic> map, BuildContext buildContext,
      ClickListener? listener) {

    var clickEvent = map["click_event"];
    var label = map["label"];
    var icon = map["icon"];
    var backgroundColor = map["backgroundColor"];

    var widget = FeatureButton(
      label: label,
      icon: icon != null ? DynamicWidgetBuilder.build(json.encode(icon), buildContext, NonResponseWidgetClickListener()) : null,
      backgroundColor: backgroundColor != null ? parseHexColor(backgroundColor) : null,
      onPressed: (){
        if (clickEvent != null){
          // listener?.onClicked();
        }
      },
    );

    return widget;
  }

  @override
  String get widgetName => "FeatureButton";

  @override
  Type get widgetType => throw UnimplementedError();
}
