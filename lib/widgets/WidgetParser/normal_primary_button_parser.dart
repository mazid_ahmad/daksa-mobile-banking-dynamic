import 'package:dmb_dynamic_home_example/widgets/Button/normal_primary_button.dart';
import 'package:dynamic_widget/dynamic_widget.dart';
import 'package:dynamic_widget/dynamic_widget/utils.dart';
import 'package:flutter/material.dart';

class NormalPrimaryButtonParser extends WidgetParser {
  final double? height;
  final double? width;
  final IconData? iconData;
  final void Function()? onPressed;

  NormalPrimaryButtonParser(
      {this.height, this.width, this.iconData, this.onPressed});

  @override
  Map<String, dynamic>? export(Widget? widget, BuildContext? buildContext) {
    throw UnimplementedError();
  }

  @override
  Widget parse(Map<String, dynamic> map, BuildContext buildContext,
      ClickListener? listener) {
    String clickEvent =
        map.containsKey("click_event") ? map['click_event'] : "";

    var label = map['label'];

    var button = NormalPrimaryButton(
      backgroundColor:
          map.containsKey('color') ? parseHexColor(map['color']) : null,
      label: label != null ? label : null,
      height: map.containsKey('height') ? map['height'] : height,
      width: map.containsKey('width') ? map['width'] : width,
      icon: iconData,
      onPressed: listener == null
          ? onPressed
          : () {
              listener.onClicked(clickEvent);
            },
    );

    return button;
  }

  @override
  String get widgetName => "NormalRaisedButton";

  @override
  Type get widgetType => throw UnimplementedError();
}
