import 'package:dmb_dynamic_home_example/themes/AppColor.dart';
import 'package:dynamic_widget/dynamic_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_card_swipper/flutter_card_swiper.dart';

class NewsContainerHomeParser extends WidgetParser{
  @override
  Map<String, dynamic>? export(Widget? widget, BuildContext? buildContext) {
    // TODO: implement export
    throw UnimplementedError();
  }

  @override
  Widget parse(Map<String, dynamic> map, BuildContext buildContext, ClickListener? listener) {


    var widget = NewsContainerHome();
    return widget;
  }

  @override
  // TODO: implement widgetName
  String get widgetName => "NewsContainerHome";

  @override
  // TODO: implement widgetType
  Type get widgetType => throw UnimplementedError();

}

class NewsContainerHome extends StatefulWidget {
  const NewsContainerHome({Key? key}) : super(key: key);

  @override
  _NewsContainerHomeState createState() => _NewsContainerHomeState();
}

class _NewsContainerHomeState extends State<NewsContainerHome> {
  final SwiperController _swiperController = new SwiperController();
  int _idx = 0;
  GlobalKey key = GlobalKey();

  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;
    return Column(
      children: [
        Container(
          padding: EdgeInsets.all(30),
          child: Column(
            children: [
              Container(
                width: screenSize.width,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Icon(Icons.new_releases_sharp,
                            color: AppColor().primary),
                        SizedBox(width: 8),
                        Text(
                          "News",
                          style: TextStyle(
                              color: AppColor().primary,
                              fontWeight: FontWeight.w600,
                              fontSize: 20),
                        )
                      ],
                    ),
                    Row(
                      children: List.generate(5, (index) {
                        return Container(
                          margin: EdgeInsets.only(left: 5),
                          height: 5,
                          width: 30,
                          decoration: BoxDecoration(
                              color: (index == _idx)
                                  ? AppColor().primary
                                  : AppColor().secondary,
                              borderRadius:
                              BorderRadius.circular(20)),
                        );
                      }),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Container(
          width: screenSize.width,
          height: 250,
          child: new Swiper(
            index: _idx,
            onIndexChanged: (idx) {
              setState(() {
                _idx = idx;
              });
            },
            physics: BouncingScrollPhysics(),
            viewportFraction: 0.9,
            itemCount: 5,
            controller: _swiperController,
            itemBuilder: (BuildContext context, int index) {
              return Hero(
                tag: 'image_detail$index',
                child: Container(
                    padding: const EdgeInsets.all(5),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(20),
                      child: Image.network(
                        "https://foto.wartaekonomi.co.id/files/arsip_foto_2020_06_12/bank_qnb_indonesia_070251_small.jpg",
                        fit: BoxFit.cover,
                      ),
                    )),
              );
            },
            loop: false,
            autoplay: true,
            onTap: (value) {},
          ),
        )
      ],
    );
  }
}
