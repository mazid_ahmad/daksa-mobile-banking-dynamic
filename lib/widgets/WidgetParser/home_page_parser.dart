import 'dart:convert';
import 'package:dmb_dynamic_home_example/pages/cubit/home_cubit.dart';
import 'package:dynamic_widget/dynamic_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomePageParser extends WidgetParser {
  @override
  Map<String, dynamic>? export(Widget? widget, BuildContext? buildContext) {
    throw UnimplementedError();
  }

  @override
  Widget parse(Map<String, dynamic> map, BuildContext buildContext,
      ClickListener? listener) {
    var screenSize = MediaQuery.of(buildContext).size;
    var child = map["child"];
    var widget = BlocBuilder<HomeCubit, HomeState>(
      builder: (context, state) {
        print(state.stateStatus);
        if (state.stateStatus == HomeStateStatus.loaded) {
          return SingleChildScrollView(
              physics: ClampingScrollPhysics(),
              child: Container(
                width: screenSize.width,
                child: child != null
                    ? DynamicWidgetBuilder.build(json.encode(child), buildContext,
                    NonResponseWidgetClickListener())
                    : Center(child: CircularProgressIndicator()),
              ));
        }
        return Container();
      },
    );

    return widget;
  }

  @override
  String get widgetName => "HomePage";

  @override
  Type get widgetType => throw UnimplementedError();
}
