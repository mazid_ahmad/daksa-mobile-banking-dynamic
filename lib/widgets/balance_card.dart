import 'package:dmb_dynamic_home_example/themes/AppColor.dart';
import 'package:dmb_dynamic_home_example/widgets/label_container.dart';
import 'package:flutter/material.dart';

class BalanceCard extends StatelessWidget {
  final String? type;
  final String? balance;
  final String? number;
  final Color? cardColor;

  const BalanceCard({Key? key, this.type, this.balance, this.number, this.cardColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(15),
      decoration: BoxDecoration(
          color: cardColor ?? AppColor().primaryVariant,
          borderRadius: BorderRadius.circular(15)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("Balance", style: TextStyle(color: AppColor().onPrimary)),
              SizedBox(height: 10),
              Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Text("IDR",
                      style: TextStyle(
                          fontSize: 16,
                          color: AppColor().onPrimary,
                          fontWeight: FontWeight.w600)),
                  SizedBox(width: 8),
                  Text(balance ?? "43.210.000",
                      style: TextStyle(
                          fontSize: 23,
                          color: AppColor().onPrimary,
                          fontWeight: FontWeight.w600))
                ],
              ),
            ],
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              LabelContainer(label: type ?? "E-Money"),
              SizedBox(height: 10),
              Text(number ?? "0812341247992",
                  style: TextStyle(
                      fontSize: 16,
                      color: AppColor().onPrimary,
                      fontWeight: FontWeight.w600)),
              SizedBox(width: 8),
            ],
          ),
        ],
      ),
    );
  }
}
