import 'package:dmb_dynamic_home_example/themes/AppColor.dart';
import 'package:flutter/material.dart';

class ExpandedButton extends StatelessWidget {
  final String? label;
  final IconData? icon;
  final Function()? onPressed;

  const ExpandedButton(
      {Key? key, @required this.label, this.icon, @required this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: GestureDetector(
        onTap: onPressed,
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
          decoration: BoxDecoration(
              color: AppColor().onPrimary,
              borderRadius: BorderRadius.circular(20)),
          child: Column(
            children: [
              Icon(
                icon,
                size: 40,
                color: AppColor().primaryVariant,
              ),
              SizedBox(height: 10),
              Text(
                label!,
                style: TextStyle(
                    fontWeight: FontWeight.w600, color: AppColor().primary),
              )
            ],
          ),
        ),
      ),
    );
  }
}
