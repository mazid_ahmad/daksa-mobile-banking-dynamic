import 'package:dmb_dynamic_home_example/themes/AppColor.dart';
import 'package:flutter/material.dart';

class TextGroup extends StatelessWidget {
  final String? label;
  final String? value;

  const TextGroup({Key? key, @required this.label, @required this.value})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(label!,
              style: TextStyle(
                  fontWeight: FontWeight.w600, color: AppColor().primary)),
          SizedBox(height: 5),
          Text(value!, style: TextStyle(fontWeight: FontWeight.w600))
        ],
      ),
    );
  }
}
