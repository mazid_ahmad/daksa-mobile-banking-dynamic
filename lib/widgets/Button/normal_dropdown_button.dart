import 'package:dmb_dynamic_home_example/config/NumberConstant.dart';
import 'package:dmb_dynamic_home_example/themes/AppColor.dart';
import 'package:dmb_dynamic_home_example/widgets/Button/abstract_button.dart';
import 'package:flutter/material.dart';

class NormalDropdownButton extends AbstractButton {
  final double? width;
  final double? height;
  final Color? color;
  final String? value;
  final List<String?>? items;
  final Function(String? value)? onChanged;
  final Function()? onTap;

  const NormalDropdownButton(
      {Key? key,
      @required Function()? onPressed,
      @required String? label,
      this.width,
      this.height,
      this.color,
      this.value,
      @required this.items,
      this.onChanged,
      this.onTap})
      : super(key: key, label: label, onPressed: onPressed);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20),
      height: height ?? NumberConstant.defaultNormalButtonHeight,
      width: width,
      decoration: BoxDecoration(
          color: color ?? AppColor().onPrimary,
          borderRadius: BorderRadius.circular(5)),
      margin: EdgeInsets.symmetric(vertical: 8),
      child: DropdownButtonHideUnderline(
        child: DropdownButton<String>(
            onTap: onTap,
            style: TextStyle(
                color: AppColor().primary,
                fontSize: 17,
                fontWeight: FontWeight.w600),
            icon: Icon(
              Icons.keyboard_arrow_down,
              color: AppColor().primary,
            ),
            isExpanded: true,
            value: value,
            hint: Text(label!),
            onChanged: onChanged,
            items: items!.map<DropdownMenuItem<String>>((String? value) {
              return DropdownMenuItem<String>(
                value: value,
                child: Text(value!),
              );
            }).toList()),
      ),
    );
  }
}
