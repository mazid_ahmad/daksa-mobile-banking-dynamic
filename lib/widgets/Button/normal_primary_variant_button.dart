import 'package:dmb_dynamic_home_example/config/NumberConstant.dart';
import 'package:dmb_dynamic_home_example/themes/AppColor.dart';
import 'package:dmb_dynamic_home_example/widgets/Button/abstract_button.dart';
import 'package:flutter/material.dart';

class NormalPrimaryVariantButton extends AbstractButton {
  final double? width;
  final double? height;

  const NormalPrimaryVariantButton(
      {Key? key,
      @required Function()? onPressed,
      @required String? label,
      this.width,
      this.height})
      : super(key: key, label: label, onPressed: onPressed);

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: BoxConstraints.tightFor(
          width: width,
          height: height ?? NumberConstant.defaultNormalButtonHeight),
      child: ElevatedButton(
        child: Text(label!),
        onPressed: this.onPressed,
        style: ElevatedButton.styleFrom(
            primary: AppColor().primaryVariant,
            elevation: 0,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(25))),
      ),
    );
  }
}
