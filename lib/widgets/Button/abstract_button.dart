import 'package:flutter/material.dart';

abstract class AbstractButton extends StatelessWidget {
  final Key? key;
  @required final void Function()? onPressed;
  final String? label;

  const AbstractButton({this.key, this.onPressed, this.label}) : super(key: key);

  @override
  Widget build(BuildContext context);
}
