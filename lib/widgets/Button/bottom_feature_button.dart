import 'package:dmb_dynamic_home_example/themes/AppColor.dart';
import 'package:flutter/material.dart';

import 'feature_button.dart';

class BottomFeatureButton extends StatelessWidget {
  final String? label;
  final IconData? iconData;
  final Function()? onPressed;

  const BottomFeatureButton({Key? key, required this.label, this.iconData, this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 100,
      height: 100,
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
          color: AppColor()
              .secondary
              .withOpacity(
              0.1),
          borderRadius:
          BorderRadius
              .circular(
              15)),
      child: FeatureButton(
          label:
          label,
          backgroundColor:
          Colors.transparent,
          icon: Icon(iconData, size: 30, color: AppColor().primary),
          onPressed: onPressed),
    );
  }
}
