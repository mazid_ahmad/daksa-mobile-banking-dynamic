import 'package:dmb_dynamic_home_example/themes/AppColor.dart';
import 'package:dmb_dynamic_home_example/widgets/Button/abstract_button.dart';
import 'package:flutter/material.dart';

class NormalOutlinePrimaryButton extends AbstractButton {
  final double? width;
  final double? height;

  const NormalOutlinePrimaryButton(
      {Key? key,
      @required Function()? onPressed,
      @required String? label,
      this.width,
      this.height})
      : super(key: key, label: label, onPressed: onPressed);

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: BoxConstraints.tightFor(width: width, height: height ?? 50),
      child: ElevatedButton(
        child: Text(label!, style: TextStyle(color: AppColor().primary)),
        onPressed: this.onPressed,
        style: ElevatedButton.styleFrom(
            primary: AppColor().onPrimary,
            elevation: 0,
            shape: RoundedRectangleBorder(
                side: BorderSide(color: AppColor().primary, width: 2),
                borderRadius: BorderRadius.circular(25))),
      ),
    );
  }
}
