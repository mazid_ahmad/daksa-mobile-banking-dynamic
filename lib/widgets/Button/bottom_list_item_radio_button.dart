import 'package:dmb_dynamic_home_example/model/transfer_type.dart';
import 'package:dmb_dynamic_home_example/themes/AppColor.dart';
import 'package:flutter/material.dart';

class BottomListItemRadioButton extends StatelessWidget {
  final String? label;
  final int? selectedValue;
  final List<TransferType>? types;
  final Function(int? value)? selected;

  const BottomListItemRadioButton(
      {Key? key,
      @required this.label,
      this.types,
      this.selectedValue,
      this.selected})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;

    void _showBottomSheetTransfer(
        {@required String? title,
        @required List<TransferType>? types,
        int? groupValue,
        @required Function(int? value)? selectedIndex}) {
      showModalBottomSheet(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
          context: context,
          builder: (context) {
            return Container(
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(title!,
                      style: TextStyle(
                          color: AppColor().primary,
                          fontSize: 22,
                          fontWeight: FontWeight.w600)),
                  SizedBox(height: 20),
                  Column(
                    children: new List<RadioListTile<int>>.generate(
                        types!.length, (int index) {
                      return new RadioListTile<int>(
                        value: index,
                        groupValue: groupValue,
                        title: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              types[index].title!,
                              style: TextStyle(
                                  color: AppColor().primary,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 16),
                            ),
                            types[index].description != null
                                ? SizedBox(height: 5)
                                : SizedBox(),
                            types[index].description != null
                                ? Text(
                                    types[index].description!,
                                    style: TextStyle(
                                        color: AppColor().grey.withOpacity(0.8),
                                        fontWeight: FontWeight.w500,
                                        fontSize: 12),
                                  )
                                : SizedBox(),
                          ],
                        ),
                        onChanged: (int? value) {
                          selectedIndex!(value!);
                          Navigator.pop(context);
                        },
                      );
                    }),
                  )
                ],
              ),
            );
          });
    }

    return GestureDetector(
      onTap: () {
        _showBottomSheetTransfer(
            title: label,
            types: types,
            groupValue: selectedValue,
            selectedIndex: selected);
      },
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 20),
        height: 60,
        width: screenSize.width,
        decoration: BoxDecoration(
            color: AppColor().onPrimary,
            borderRadius: BorderRadius.circular(5)),
        margin: EdgeInsets.symmetric(vertical: 8),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              selectedValue != null ? types![selectedValue!].title! : label!,
              style: TextStyle(
                  color: selectedValue != null
                      ? AppColor().primary
                      : AppColor().grey,
                  fontWeight: FontWeight.w500,
                  fontSize: 16),
            ),
            Icon(Icons.keyboard_arrow_down, color: AppColor().primary)
          ],
        ),
      ),
    );
  }
}
