import 'package:dmb_dynamic_home_example/themes/AppColor.dart';
import 'package:flutter/material.dart';

import 'abstract_button.dart';

class FeatureButton extends AbstractButton {
  final Widget? icon;
  final Color? backgroundColor;
  const FeatureButton(
      {Key? key,
      required String? label,
      Function()? onPressed,
      this.icon,
      this.backgroundColor})
      : super(key: key, label: label, onPressed: onPressed);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: backgroundColor ?? AppColor().onPrimary,
      child: InkWell(
        onTap: this.onPressed,
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              if (icon != null) icon!,
              if (icon != null) SizedBox(height: 5),
              Text(label!,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: AppColor().primary, fontWeight: FontWeight.w600))
            ],
          ),
        ),
      ),
    );
  }
}
