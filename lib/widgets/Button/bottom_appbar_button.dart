import 'package:dmb_dynamic_home_example/themes/AppColor.dart';
import 'package:flutter/material.dart';

class BottomAppbarButton extends StatelessWidget {
  final String? label;
  final IconData? iconData;
  final void Function()? onPressed;

  const BottomAppbarButton(
      {Key? key, this.label, this.iconData, this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      child: Container(
        height: 60,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(iconData, color: AppColor().onPrimary),
            label != null
                ? Text(label!,
                    style: TextStyle(
                        fontSize: 12,
                        color: AppColor().onPrimary,
                        fontWeight: FontWeight.w300))
                : Container()
          ],
        ),
      ),
    );
  }
}
