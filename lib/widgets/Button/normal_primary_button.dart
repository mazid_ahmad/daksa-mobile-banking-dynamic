import 'package:dmb_dynamic_home_example/config/NumberConstant.dart';
import 'package:dmb_dynamic_home_example/themes/AppColor.dart';
import 'package:dmb_dynamic_home_example/widgets/Button/abstract_button.dart';
import 'package:flutter/material.dart';

class NormalPrimaryButton extends AbstractButton {
  final double? width;
  final double? height;
  final Color? backgroundColor;
  final IconData? icon;

  const NormalPrimaryButton(
      {Key? key,
      @required Function()? onPressed,
      @required String? label,
      this.width,
      this.height,
      this.backgroundColor,
      this.icon})
      : super(key: key, label: label, onPressed: onPressed);

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: BoxConstraints.tightFor(
          width: width,
          height: height ?? NumberConstant.defaultNormalButtonHeight),
      child: icon != null
          ? ElevatedButton.icon(
              icon: Icon(icon),
              label: Text(label!),
              onPressed: this.onPressed,
              style: ElevatedButton.styleFrom(
                  primary: backgroundColor ?? AppColor().primary,
                  elevation: 0,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(25))),
            )
          : ElevatedButton(
              child: Text(label!),
              onPressed: this.onPressed,
              style: ElevatedButton.styleFrom(
                  primary: backgroundColor ?? AppColor().primary,
                  elevation: 0,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(25))),
            ),
    );
  }
}
