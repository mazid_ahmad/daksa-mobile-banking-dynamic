import 'package:dmb_dynamic_home_example/themes/AppColor.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class DatePickerButton extends StatefulWidget {
  final String? label;
  final DateTime? selectedValue;
  final Function(DateTime date) onDone;

  const DatePickerButton(
      {Key? key, required this.label, this.selectedValue, required this.onDone})
      : super(key: key);

  @override
  _DatePickerButtonState createState() => _DatePickerButtonState();
}

class _DatePickerButtonState extends State<DatePickerButton> {
  DateTime? _selectedDate;
  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;

    void _showBottomDatePicker({@required String? title}) {
      showModalBottomSheet(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
          context: context,
          builder: (context) {
            return Container(
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
              width: screenSize.width,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      SizedBox(width: 80),
                      Text(title!,
                          style: TextStyle(
                              color: AppColor().primary,
                              fontSize: 22,
                              fontWeight: FontWeight.w600)),
                      SizedBox(
                          width: 80,
                          child: TextButton(
                              onPressed: () {
                                if (_selectedDate != null) {
                                  widget.onDone(_selectedDate!);
                                } else {
                                  widget.onDone(DateTime.now());
                                }
                                Navigator.pop(context);
                              },
                              child: Text("Done")))
                    ],
                  ),
                  SizedBox(height: 20),
                  Container(
                      height: 150,
                      child: CupertinoDatePicker(
                        onDateTimeChanged: (value) {
                          setState(() {
                            _selectedDate = value;
                          });
                        },
                        mode: CupertinoDatePickerMode.date,
                        maximumDate: DateTime.now(),
                        maximumYear: DateTime.now().year,
                        initialDateTime:
                            DateTime.now().add(Duration(hours: -1)),
                      ))
                ],
              ),
            );
          });
    }

    return GestureDetector(
      onTap: () {
        _showBottomDatePicker(title: widget.label);
      },
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 20),
        height: 50,
        width: screenSize.width,
        decoration: BoxDecoration(
            color: AppColor().onPrimary,
            borderRadius: BorderRadius.circular(5)),
        margin: EdgeInsets.symmetric(vertical: 8),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              widget.selectedValue != null
                  ? DateFormat("yyyy-MM-dd").format(widget.selectedValue!)
                  : widget.label ?? "",
              style: TextStyle(
                  color: widget.selectedValue != null
                      ? AppColor().primary
                      : AppColor().grey,
                  fontWeight: FontWeight.w500,
                  fontSize: 16),
            ),
            Icon(Icons.keyboard_arrow_down, color: AppColor().primary)
          ],
        ),
      ),
    );
  }
}
