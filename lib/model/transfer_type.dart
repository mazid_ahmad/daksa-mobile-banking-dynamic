import 'dart:convert';

class TransferType {
  String? title;
  String? description;

  TransferType({
    required this.title,
    this.description,
  });

  TransferType copyWith({
    String? title,
    String? description,
  }) {
    return TransferType(
      title: title ?? this.title,
      description: description ?? this.description,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'title': title,
      'description': description,
    };
  }

  factory TransferType.fromMap(Map<String, dynamic> map) {
    return TransferType(
      title: map['title'],
      description: map['description'],
    );
  }

  String toJson() => json.encode(toMap());

  factory TransferType.fromJson(String source) =>
      TransferType.fromMap(json.decode(source));

  @override
  String toString() => 'TransferType(title: $title, description: $description)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is TransferType &&
        other.title == title &&
        other.description == description;
  }

  @override
  int get hashCode => title.hashCode ^ description.hashCode;
}
