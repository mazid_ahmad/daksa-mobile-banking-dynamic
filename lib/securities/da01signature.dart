import 'dart:convert';
import 'package:crypto/crypto.dart';
import 'package:dmb_dynamic_home_example/config/base_config.dart';

class DA01signature {
  Future<String> getSignature(
      {required String method,
      dynamic contentString,
      String? contentType,
      required String date,
      required String canonicalPath}) async{

    var _stringToSign = await _createStringToSign(
        method: method,
        date: date,
        contentType: contentType,
        canonicalPath: canonicalPath,
        contentString: contentString);

    return await _generateSignature(sign: _stringToSign);
  }

  Future<String> _createStringToSign(
      {required String method,
      String? contentString,
      String? contentType,
      required String date,
      required String canonicalPath}) async{

    String _stringToSign = method;
    if (method == "POST" || method == "PUT") {
      if (contentString != null && contentString != ""){
        var _content = utf8.encode(contentString);
        var _hashContent = md5.convert(_content).toString();
        _stringToSign += "\n$_hashContent";
      }
      if (contentString != null && contentString != ""){
        _stringToSign += "\n$contentType";
      }
    }
    _stringToSign += "\n$date\n$canonicalPath";
    return _stringToSign;
  }

  Future<String> _generateSignature({required String sign}) async{
    var _keyBytes = utf8.encode(BaseConfig.SECRET_KEY);
    var _signBytes = utf8.encode(sign);
    var _hmacSha1key = Hmac(sha1, _keyBytes);
    var _digest = _hmacSha1key.convert(_signBytes);
    return base64.encode(_digest.bytes);
  }
}
