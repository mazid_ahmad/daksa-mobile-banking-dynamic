import 'package:dmb_dynamic_home_example/shared/api/widget_service.dart';
import 'package:dmb_dynamic_home_example/shared/response_server.dart';

class WidgetRepository {
  final WidgetService _widgetService = WidgetService();

  Future<ResponseServer> getHomeWidget() async{
    return await _widgetService.getHomeWidget();
  }
}