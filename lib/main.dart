import 'package:dmb_dynamic_home_example/bloc_observer.dart';
import 'package:dmb_dynamic_home_example/pages/home_page.dart';
import 'package:dmb_dynamic_home_example/shared/routers.dart';
import 'package:dmb_dynamic_home_example/shared/utils/DynamicWidgetUtil.dart';
import 'package:dmb_dynamic_home_example/themes/AppTheme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart';

import 'pages/cubit/home_cubit.dart';

Future<void> main() async{
  // var appDir = await getApplicationDocumentsDirectory();
  // Hive.init(appDir.path);
  DynamicWidgetUtil().addWidgetParser();
  // await DynamicWidgetUtil().syncWidgetFromApi();
  Bloc.observer = Observer();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Flutter Demo',
      theme: AppTheme.light,
      home: BlocProvider<HomeCubit>(
        create: (context) => HomeCubit(),
        child: HomePage(),
      ),
      getPages: Routers().routes,
    );
  }
}
