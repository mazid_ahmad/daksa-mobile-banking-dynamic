import 'dart:convert';
import 'dart:io';

import 'package:dmb_dynamic_home_example/repositories/widget_repository.dart';
import 'package:dmb_dynamic_home_example/shared/click_listener.dart';
import 'package:dmb_dynamic_home_example/shared/storage/storage.dart';
import 'package:dmb_dynamic_home_example/widgets/WidgetParser/balance_card_parser.dart';
import 'package:dmb_dynamic_home_example/widgets/WidgetParser/custom_home_clip_path_parser.dart';
import 'package:dmb_dynamic_home_example/widgets/WidgetParser/custom_text_parser.dart';
import 'package:dmb_dynamic_home_example/widgets/WidgetParser/feature_button_parser.dart';
import 'package:dmb_dynamic_home_example/widgets/WidgetParser/feature_card_home_parser.dart';
import 'package:dmb_dynamic_home_example/widgets/WidgetParser/home_header_parser.dart';
import 'package:dmb_dynamic_home_example/widgets/WidgetParser/home_page_parser.dart';
import 'package:dmb_dynamic_home_example/widgets/WidgetParser/label_container_parser.dart';
import 'package:dmb_dynamic_home_example/widgets/WidgetParser/news_container_home_parser.dart';
import 'package:dmb_dynamic_home_example/widgets/WidgetParser/normal_primary_button_parser.dart';
import 'package:dynamic_widget/dynamic_widget.dart';
import 'package:dynamic_widget/dynamic_widget/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';

class DynamicWidgetUtil {
  final WidgetRepository _widgetRepository = WidgetRepository();
  final Storage _storage = Storage();

  void addWidgetParser() {
    DynamicWidgetBuilder.addParser(FeatureButtonParser());
    DynamicWidgetBuilder.addParser(BalanceCardParser());
    DynamicWidgetBuilder.addParser(CustomTextParser());
    DynamicWidgetBuilder.addParser(NormalPrimaryButtonParser());
    DynamicWidgetBuilder.addParser(LabelContainerParser());
    DynamicWidgetBuilder.addParser(FeatureCardHomeParser());
    DynamicWidgetBuilder.addParser(NewsContainerHomeParser());
    DynamicWidgetBuilder.addParser(CustomHomeClipPathParser());
    DynamicWidgetBuilder.addParser(HomeHeaderParser());
    DynamicWidgetBuilder.addParser(HomePageParser());
  }

  Future<Widget?> convertDynamicWidget(
      BuildContext context, String path) async {
    Directory appDocumentsDirectory =
        await getApplicationDocumentsDirectory(); // 1
    String appDocumentsPath = appDocumentsDirectory.path; // 2
    File file = File("$appDocumentsPath/$path"); // 1
    String fileContent = await file.readAsString();
    // print(fileContent);
    // var jsonWidget = await rootBundle.loadString("assets/widgets/$path");
    return DynamicWidgetBuilder.build(
        fileContent, context, DefaultClickListener());
  }

  Future<Widget?> convertDynamicWidgetFromStorage(
      BuildContext context, String key) async {
    // Directory appDocumentsDirectory =
    //     await getApplicationDocumentsDirectory(); // 1
    // String appDocumentsPath = appDocumentsDirectory.path; // 2
    // File file = File("$appDocumentsPath/$path"); // 1
    // String fileContent = await file.readAsString();
    // print(fileContent);
    // var jsonWidget = await rootBundle.loadString("assets/widgets/$path");
    var jsonWidget = await _storage.getData(boxName: "widget", key: key);
    return DynamicWidgetBuilder.build(
        jsonWidget, context, DefaultClickListener());
  }

  Future<Widget?> convertDynamicWidgetJson(
      BuildContext context, String json) async {
    return DynamicWidgetBuilder.build(json, context, DefaultClickListener());
  }

  Future<Color?> parseJsonColor(String key) async {
    var jsonColor = await rootBundle.loadString("assets/widgets/colors.json");
    return parseHexColor(json.decode(jsonColor)[key]);
  }

  Future<Widget?> convertDynamicWidgetParser(
      BuildContext context, String path, WidgetParser parser) async {
    Directory appDocumentsDirectory =
        await getApplicationDocumentsDirectory(); // 1
    String appDocumentsPath = appDocumentsDirectory.path; // 2
    File file = File("$appDocumentsPath/$path"); // 1
    String fileContent = await file.readAsString();
    print(fileContent);

    DynamicWidgetBuilder.addParser(parser);
    return DynamicWidgetBuilder.build(
        fileContent, context, DefaultClickListener());
  }

  Future syncWidgetFromApi() async {
    try {
      var response = await _widgetRepository.getHomeWidget();

      Directory appDocumentsDirectory = await getApplicationDocumentsDirectory(); // 1
      String appDocumentsPath = appDocumentsDirectory.path; // 2
      String filePath = '$appDocumentsPath/home_widget.json';

      File file = File(filePath); // 1
      await file.writeAsString(json.encode(response.data));

      // await _storage.putString(
      //     boxName: "widget",
      //     key: "homeWidget",
      //     value: json.encode(response.data));
    } catch (e) {
      print(e.toString());
    }
  }
}
