import 'package:intl/intl.dart';

class DateUtil{
  String convertDateForRequest(DateTime date) {
    final DateFormat _format = DateFormat('EEE, d MMM yyyy HH:mm:ss');
    return _format.format(date);
  }
}