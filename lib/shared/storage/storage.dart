import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:hive/hive.dart';

enum StorageConstant { userInfo, userAccount }

class Storage {
  Storage();

  Future<void> putJson(
      {required String boxName, required Map<String, dynamic> json}) async {
    List<int> hiveKey = await hiveChipperKey;
    Box box =
        await Hive.openBox(boxName, encryptionCipher: HiveAesCipher(hiveKey));
    await box.putAll(json);
    await box.close();
    return;
  }

  Future<void> putData(
      {required String boxName,
      required String key,
      required Map<String, dynamic> data}) async {
    List<int> hiveKey = await hiveChipperKey;
    Box box =
        await Hive.openBox(boxName, encryptionCipher: HiveAesCipher(hiveKey));
    final String stringJson = json.encode(data);
    await box.put(key, stringJson);
    await box.close();
    return;
  }

  Future<void> putString(
      {required String boxName,
        required String key,
        required String value}) async {
    List<int> hiveKey = await hiveChipperKey;
    Box box =
    await Hive.openBox(boxName, encryptionCipher: HiveAesCipher(hiveKey));
    await box.put(key, value);
    await box.close();
    return;
  }

  Future<void> putInt(
      {required String boxName,
        required String key,
        required int value}) async {
    List<int> hiveKey = await hiveChipperKey;
    Box box =
    await Hive.openBox(boxName, encryptionCipher: HiveAesCipher(hiveKey));
    await box.put(key, value);
    await box.close();
    return;
  }

  Future<void> putBool(
      {required String boxName,
        required String key,
        required bool value}) async {
    List<int> hiveKey = await hiveChipperKey;
    Box box =
    await Hive.openBox(boxName, encryptionCipher: HiveAesCipher(hiveKey));
    await box.put(key, value);
    await box.close();
    return;
  }

  Future<void> updateBox(
      {required String boxName,
        required Map<String, dynamic> value,
        required int index}) async {
    List<int> hiveKey = await hiveChipperKey;
    Box box =
    await Hive.openBox(boxName, encryptionCipher: HiveAesCipher(hiveKey));
    await box.putAt(index, value);
    await box.close();
    return;
  }

  Future<dynamic> getData(
      {required String boxName,
        required String key}) async {
    List<int> hiveKey = await hiveChipperKey;
    Box box =
    await Hive.openBox(boxName, encryptionCipher: HiveAesCipher(hiveKey));
    final value = await box.get(key);
    await box.close();
    return value;
  }

  Future<DateTime?> getDate(
      {required String boxName,
        required String key}) async {
    List<int> hiveKey = await hiveChipperKey;
    Box box =
    await Hive.openBox(boxName, encryptionCipher: HiveAesCipher(hiveKey));
    DateTime? date = await box.get(key);
    await box.close();
    return date;
  }

  Future<int?> getInt(
      {required String boxName,
        required String key}) async {
    List<int> hiveKey = await hiveChipperKey;
    Box box =
    await Hive.openBox(boxName, encryptionCipher: HiveAesCipher(hiveKey));
    int? value = await box.get(key);
    await box.close();
    return value;
  }

  Future<bool?> getBool(
      {required String boxName,
        required String key}) async {
    List<int> hiveKey = await hiveChipperKey;
    Box box =
    await Hive.openBox(boxName, encryptionCipher: HiveAesCipher(hiveKey));
    bool? value = await box.get(key);
    await box.close();
    return value;
  }

  Future<double?> getDouble(
      {required String boxName,
        required String key}) async {
    List<int> hiveKey = await hiveChipperKey;
    Box box =
    await Hive.openBox(boxName, encryptionCipher: HiveAesCipher(hiveKey));
    double? value = await box.get(key);
    await box.close();
    return value;
  }

  Future<dynamic> getList(
      {required String boxName,
        required String key}) async {
    List<int> hiveKey = await hiveChipperKey;
    Box box =
    await Hive.openBox(boxName, encryptionCipher: HiveAesCipher(hiveKey));
    dynamic value = await box.get(key);
    await box.close();
    return value;
  }

  Future<void> deleteData(
      {required String boxName,
        required String key}) async {
    List<int> hiveKey = await hiveChipperKey;
    Box box =
    await Hive.openBox(boxName, encryptionCipher: HiveAesCipher(hiveKey));
    await box.deleteFromDisk();
    await box.close();
    return;
  }

  Future<List<int>> get hiveChipperKey async {
    final ss = FlutterSecureStorage();
    List<int> hiveKey = [];

    try {
      String? stringKey = await ss.read(key: 'keyDMB');

      if (stringKey != null) {
        hiveKey = stringKey.codeUnits;
      } else {
        hiveKey = Hive.generateSecureKey();
        Uint8List bytes = Uint8List.fromList(hiveKey);
        stringKey = String.fromCharCodes(bytes);

        await ss.write(key: 'keyDMB', value: stringKey);
      }
    } catch (e) {
      await ss.delete(key: 'boxKey');
    }

    return hiveKey;
  }
}
