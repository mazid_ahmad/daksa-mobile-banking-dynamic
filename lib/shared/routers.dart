import 'package:dmb_dynamic_home_example/pages/home_page.dart';
import 'package:get/get.dart';

class Routers {
  static const String home = "/home";

  List<GetPage> routes = [
    GetPage(
        name: home,
        page: () => HomePage())
  ];
}