import 'package:dmb_dynamic_home_example/shared/routers.dart';
import 'package:dynamic_widget/dynamic_widget.dart';
import 'package:get/get.dart';

class DefaultClickListener extends ClickListener{
  @override
  void onClicked(String? event) {
    switch(event){
      case "home": Get.toNamed(Routers.home); break;
      default: break;
    }
  }
}