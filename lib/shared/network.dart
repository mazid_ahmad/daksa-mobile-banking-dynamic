import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:dmb_dynamic_home_example/config/base_config.dart';
import 'package:dmb_dynamic_home_example/securities/da01signature.dart';
import 'package:dmb_dynamic_home_example/shared/network_exception.dart';
import 'package:dmb_dynamic_home_example/shared/response_server.dart';
import 'package:dmb_dynamic_home_example/shared/utils/DateUtil.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

enum Method { DELETE, PUT, POST, GET }

class Network extends Object with DA01signature {
  late Dio _dio;

  Network() {
    BaseOptions options = new BaseOptions(baseUrl: BaseConfig.URL_SERVER);
    this._dio = Dio(options);
    this._dio.interceptors.add(PrettyDioLogger(
        requestBody: true,
        requestHeader: true,
        responseHeader: true,
        maxWidth: 180));
  }

  Future<dynamic> request(String method, String path,
      {required String contentType,
      Map<String, dynamic>? queryParams,
      dynamic content}) async {
    late Response responseJson;

    var _data = json.encode(content);
    var _now = DateUtil().convertDateForRequest(DateTime.now());
    var _signature = await getSignature(
        method: method,
        date: _now,
        canonicalPath: path,
        contentString: _data,
        contentType: contentType);

    var authorization = "DA01 ${BaseConfig.API_KEY}:$_signature";
    var headers = {'Date': _now, 'Authorization': authorization};

    if (method == 'POST' || method == 'PUT') {
      headers['Content-Type'] = contentType;
      headers['Accept'] = contentType;
    }

    _dio.options.headers = headers;
    _dio.options.contentType = contentType;
    _dio.options.connectTimeout = 8000;
    _dio.options.receiveTimeout = 8000;

    try {
      if (method == 'POST') {
        responseJson =
            await _dio.post(path, data: _data, queryParameters: queryParams);
      } else if (method == 'PUT') {
        responseJson =
            await _dio.put(path, data: _data, queryParameters: queryParams);
      } else if (method == 'DELETE') {
        await _dio.delete(path, queryParameters: queryParams);
      } else {
        responseJson = await _dio.get(path, queryParameters: queryParams);
      }

      if (responseJson.statusCode! >= 200 && responseJson.statusCode! <= 300) {
        return ResponseServer(
            data: responseJson.data,
            header: responseJson.headers,
            statusCode: responseJson.statusCode);
      }
    } on DioError catch (e) {
      _returnResponseError(e);
    }
  }

  dynamic _returnResponseError(DioError e) {
    String messageDefault =
        'Koneksi Terputus.\nSilahkan coba beberapa saat lagi';

    switch (e.type) {
      case DioErrorType.connectTimeout:
      case DioErrorType.sendTimeout:
      case DioErrorType.receiveTimeout:
        throw NetworkException(
            httpStatus: e.response?.statusCode,
            responseCode: e.response?.headers.value('responseCode'),
            responseMessage: e.response?.statusMessage ?? messageDefault);
      case DioErrorType.response:
        if (e.response?.statusCode == 404) {
          String? _message = '';
          if (e.response?.statusMessage == null) {
            _message = 'Not Found';
          } else {
            _message = e.response?.statusMessage;
          }
          throw NetworkException(
              httpStatus: e.response?.statusCode,
              responseCode: e.response?.headers.value('responseCode'),
              responseMessage: '404 - $_message');
        }
        if (e.response?.statusCode == 502) {
          throw NetworkException(
              httpStatus: e.response?.statusCode,
              responseCode: e.response?.headers.value('responseCode'),
              responseMessage: 'FORBIDDEN ACCESS');
        }
        if (e.response?.statusCode == 500) {
          throw NetworkException(
              httpStatus: e.response?.statusCode,
              responseCode: e.response?.headers.value('responseCode'),
              responseMessage:
                  e.response?.statusMessage ?? 'Internal Server Error');
        }
        throw NetworkException(
            httpStatus: e.response?.statusCode,
            responseCode: e.response?.headers.value('responseCode'),
            responseMessage:
                e.response?.headers.value('responseMessage') != null
                    ? e.response?.headers.value('responseMessage')
                    : (e.response?.data != null ? '' : e.response?.data));
      case DioErrorType.cancel:
        throw 'Cancel';
      case DioErrorType.other:
        if (e.message.contains('SocketException')){
          throw NetworkException(responseMessage: 'checkIntenrnetException');
        }
        throw 'Maaf, sedang mengalami gangguan ' + e.response!.statusCode.toString();
    }
  }
}
