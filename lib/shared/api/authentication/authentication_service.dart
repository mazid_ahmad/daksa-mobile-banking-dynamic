import 'package:dmb_dynamic_home_example/config/base_config.dart';
import 'package:dmb_dynamic_home_example/config/canonical_path.dart';
import 'package:dmb_dynamic_home_example/shared/api/authentication/i_authentication_service.dart';
import 'package:dmb_dynamic_home_example/shared/network.dart';
import 'package:dmb_dynamic_home_example/shared/response_server.dart';

class AuthenticationService extends Network implements IAuthenticationService {

  @override
  Future<ResponseServer> phoneAuth(String phoneNumber) async {
    var _data = {
      "phoneNumber": phoneNumber,
    };

    var response = await request("POST", CanonicalPath.LOGIN_BY_OTP,
        content: _data, contentType: BaseConfig.contentType);

    return response;
  }

  @override
  Future<ResponseServer> verifyOtp(String phoneNumber, String otp) async {
    var _data = {"phoneNumber": phoneNumber, "token": otp};

    var response = await request("POST", CanonicalPath.LOGIN_BY_OTP,
        contentType: BaseConfig.contentType, content: _data);

    return response;
  }
}
