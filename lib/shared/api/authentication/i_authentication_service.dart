import 'package:dmb_dynamic_home_example/shared/response_server.dart';

abstract class IAuthenticationService {
  Future<ResponseServer> phoneAuth(String phoneNumber);
  Future<ResponseServer> verifyOtp(String phoneNumber, String otp);
}