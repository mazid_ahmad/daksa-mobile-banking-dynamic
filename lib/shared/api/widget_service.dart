import 'package:dmb_dynamic_home_example/config/base_config.dart';
import 'package:dmb_dynamic_home_example/config/canonical_path.dart';

import '../network.dart';
import '../response_server.dart';

class WidgetService extends Network {
  Future<ResponseServer> getHomeWidget() async {
    var response = await request("POST",
        "${CanonicalPath.HOME_WIDGET}?fileRepoId=81a8335671ea495888e224f479cc287c",
        contentType: BaseConfig.contentType);

    return response;
  }
}
