import 'package:dmb_dynamic_home_example/pages/cubit/home_cubit.dart';
import 'package:dmb_dynamic_home_example/shared/utils/DynamicWidgetUtil.dart';
import 'package:dmb_dynamic_home_example/themes/AppAssetImage.dart';
import 'package:dmb_dynamic_home_example/themes/AppColor.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  late HomeCubit _homeCubit;

  @override
  void initState() {
    super.initState();
    _homeCubit = BlocProvider.of<HomeCubit>(context);
    _homeCubit.initHome();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;
    return Scaffold(
        floatingActionButton: FloatingActionButton(
            onPressed: () async {
              _homeCubit.initHome();
            },
            child: Icon(Icons.sync)),
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          backgroundColor: AppColor().primary,
          elevation: 0,
          centerTitle: true,
          title: Image(image: AppAssetImage.dmbLogoWhite),
        ),
        body: BlocBuilder<HomeCubit, HomeState>(
          builder: (context, state) {
            print(state.stateStatus);
            if (state.stateStatus == HomeStateStatus.loaded) {
              return FutureBuilder<Widget?>(
                  future: DynamicWidgetUtil()
                      .convertDynamicWidget(context, "home_widget.json"),
                  builder: (context, snapshot) {
                    if (snapshot.hasError) {
                      print(snapshot.error);
                    }
                    return snapshot.hasData
                        ? SizedBox.expand(
                            child: snapshot.data,
                          )
                        : Text("Loading...");
                  });
            }
            return Center(child: CircularProgressIndicator());
          },
        ));
  }
}
