part of 'home_cubit.dart';

enum HomeStateStatus { initial, loading, loaded, failed }

class HomeState extends Equatable {
  final HomeStateStatus stateStatus;
  final String? name;
  final String? greeting;
  final String? account;
  final String? type;
  final String? balance;
  final String? jsonWidget;

  const HomeState(
      {required this.stateStatus,
      this.name,
      this.greeting,
      this.account,
      this.type,
      this.balance,
      this.jsonWidget});

  HomeState update(
          {required HomeStateStatus stateStatus,
          String? name,
          String? greeting,
          String? account,
          String? type,
          String? balance,
          String? jsonWidget}) =>
      copyWith(
          balance: balance,
          name: name,
          greeting: greeting,
          account: account,
          type: type,
          stateStatus: stateStatus,
          jsonWidget: jsonWidget);

  HomeState copyWith(
          {HomeStateStatus? stateStatus,
          String? name,
          String? greeting,
          String? account,
          String? type,
          String? balance,
          String? jsonWidget}) =>
      HomeState(
          stateStatus: stateStatus ?? this.stateStatus,
          name: name ?? this.name,
          greeting: greeting ?? this.greeting,
          account: account ?? this.name,
          type: type ?? this.type,
          balance: balance ?? this.balance,
          jsonWidget: jsonWidget ?? this.jsonWidget);

  @override
  List<Object?> get props => [stateStatus];
}
