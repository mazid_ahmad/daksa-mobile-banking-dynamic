import 'dart:convert';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:dmb_dynamic_home_example/repositories/widget_repository.dart';
import 'package:equatable/equatable.dart';
import 'package:path_provider/path_provider.dart';

part 'home_state.dart';

class HomeCubit extends Cubit<HomeState> {
  final WidgetRepository _widgetRepository = WidgetRepository();
  HomeCubit() : super(HomeState(stateStatus: HomeStateStatus.initial));

  void initHome() async {
    try{
      emit(state.update(stateStatus: HomeStateStatus.loading));
      emit(state.update(stateStatus: HomeStateStatus.loaded,
          name: "Mazid Ahmad",
          type: "E-Money",
          account: "095556748979749",
          balance: "42.898.442",
          greeting: "Good Morning"));
    }catch (e){
      emit(state.update(stateStatus: HomeStateStatus.failed));
    }
  }
}
